(ns basics.core
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [ring.adapter.jetty :as j]
            [ring.middleware.resource :as res]
            [hiccup.core :as h])
  (:gen-class))

(defn read-people []
  (let [people (slurp (io/resource "people.csv"))
        people (str/split people #"\n")
        people (map
                 (fn [line]
                   (str/split line #","))
                 people)
        header (first people)
        people (rest people)
        people (map
                 (fn [line]
                   (zipmap header line))
                 people)]
    people))

(defn main-page [request]
  (let [people (read-people)
        uri (:uri request)
        people (if (= uri "/")
                 people
                 (filter
                   (fn [person]
                     (.contains uri (get person "country")))
                   people))]
    (when-not (empty? people)
      (h/html [:html
               [:body
                [:table {:id "people"}
                 [:thead
                  [:tr
                   [:th "First Name"]
                   [:th "Last Name"]
                   [:th "Email"]
                   [:th "Country"]]]
                 [:tbody
                  (map
                    (fn [person]
                      [:tr
                       [:td (get person "first_name")]
                       [:td (get person "last_name")]
                       [:td (get person "email")]
                       [:td [:a {:href (get person "country")} (get person "country")]]])
                    people)]]
                [:script {:src "/main.js"}]]]))))

(defn handler [request]
  {:status 200
   :body (main-page request)})

(defn -main []
  (j/run-jetty (res/wrap-resource handler "public") {:port 3000 :join? false}))

