(set-env!
  :source-paths #{"src"}
  :resource-paths #{"resources"}
  :dependencies '[[nightlight "1.9.2"]
                  [org.clojure/clojure "1.8.0"]
                  [ring "1.5.1"]
                  [hiccup "1.0.5"]
                  [adzerk/boot-cljs "2.1.4" :scope "test"]
                  [adzerk/boot-reload "0.5.2" :scope "test"]
                  [org.clojure/clojurescript "1.9.946" :scope "test"]])

(require
  '[adzerk.boot-cljs :refer [cljs]]
  '[adzerk.boot-reload :refer [reload]]
  '[nightlight.boot :refer [nightlight]]
  'basics.core)

(deftask run []
  (comp
    (with-pass-thru _
      (basics.core/-main))
    (watch)
    (reload :asset-path "public")
    (cljs :source-map true :optimizations :none :compiler-options {:asset-path "main.out"})
    (target)
    (nightlight :port 4000 :url "http://localhost:3000")))

(deftask build []
  (comp
    (cljs :optimizations :advanced)
    (aot :namespace #{'basics.core})
    (uber)
    (jar :main 'basics.core)
    (target)))

